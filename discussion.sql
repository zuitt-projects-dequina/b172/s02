/* NOTES

SQL - Structured Query Language
    -  is used to communicate with a database
    - it is the standard language for relational database management systems
    - SQL statements are used to perform tasks such as update data on a database, or retrieve data from a database.


 Difference between SQL & MySQL
    > SQL is a query programming language that manages RDBMS. MySQL is a relational database management system that uses SQL.

    > SQL is primarily used to query and operate database systems. MySQL allows you to handle, store, modify and delete data and store data in an organized way
*/


CREATE TABLE users (
    id 
    INT (interger) 
    NOT NULL (so that it cant be null) 
    AUTO_INCREMENT (automatically add increments on the ID),
    username VARCHAR(50) (setting of character numbers)
);

DELETE RESTRICT - the only deleted key in on the parent table. not on the references keys

-- List down the databases inside the DBMS.
SHOW DATBASES;

-- Create a database.
CREATE DATABASE music_db;

-- Select a database.
USE music_db;

-- Create tables
-- Table columns have the following format: [column_name] [data_type] [other_options]
CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    pssword VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50) NOT NULL,
    address VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE albums (
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_albums_artist_id
        FOREIGN KEY (artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL,
    genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_album_id
        FOREIGN KEY(album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists_songs (
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_songs_playlist_id
        FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_playlists_songs_song_id
        FOREIGN KEY (song_id) REFERENCES songs(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

-- CONSTRAINT option -> used to specify rules for the data in a table. Constraints are used to limit the type of data that can go into a table.

-- UPDATE CASCADE -> used by the referencing rows that are updated in the child table when the referenced row is updated in the parent table which has a primary key.

-- DELETE CASCADE -> deletes the referencing rows in the child table when the referenced row is deleted in the parent table which has a primary key.
